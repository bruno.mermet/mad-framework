package fr.greyc.mad.framework.engine.agents;

import fr.greyc.mad.framework.model.Action;

public interface AgentMind {
	public Action askAction();
}
