package fr.greyc.mad.framework.model;

import java.util.Objects;

public class ArtifactID {
	private String artifactID;
	
	public ArtifactID(String id) {
		artifactID = id;
	}
	
	public String getID() {
		return artifactID;
	}

	@Override
	public int hashCode() {
		return Objects.hash(artifactID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtifactID other = (ArtifactID) obj;
		return Objects.equals(artifactID, other.artifactID);
	}
	
}
