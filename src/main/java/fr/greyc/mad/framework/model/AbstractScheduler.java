package fr.greyc.mad.framework.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.concurrent.TimeoutException;

public abstract class AbstractScheduler implements IScheduler {
	private Map<AgentMindID, AgentBodyID> embodiment;
	private Environment environment;
	private IDirectoryAdapter adapter;

	@Override
	public List<Action> getActions(AgentMindID mind) {
		List<Action> retour = environment.getEnvironmentActions();
		AgentBodyID bodyID = embodiment.get(mind);
		List<Action> bodyActions = adapter.getActions(bodyID);
		retour.addAll(bodyActions);
		return retour;
	}
	
	@Override
	public List<Observation<?>> getObservations(AgentMindID mind) {
		List<Observation<?>> retour = environment.getEnvironmentObservations();
		AgentBodyID bodyID = embodiment.get(mind);
		List<Observation<?>> bodyActions = adapter.getObservations(bodyID);
		retour.addAll(bodyActions);
		return retour;
	}

	@Override
	public void setAdapter(IDirectoryAdapter myDirectoryAdapter) {
		adapter = myDirectoryAdapter;
	}

	@Override
	public void register(Environment myEnvironment) {
		// TODO Auto-generated method stub

	}

	@Override
	public void register(AgentMindID mind, AgentBodyID body)
			throws MissingResourceException, SecurityException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isFound(AgentBodyID body) throws MissingResourceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnvironmentReady() throws MissingResourceException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAllAgentMindsReady(int delay) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAgentMindReady(AgentMindID mind, int delay) throws MissingResourceException, TimeoutException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Action requestAction(AgentMindID mind, AgentBodyID body, int delay)
			throws MissingResourceException, SecurityException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<String, Action> getNextJointAction() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void executeJointAction(HashMap<AgentBodyID, Action> myJointAction) throws MissingResourceException {
		// TODO Auto-generated method stub

	}

	@Override
	public HashMap<AgentBodyID, Set<Observation>> getObservations() throws MissingResourceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void distributeObservations(HashMap<AgentBodyID, Set<Observation>> myObservations) {
		// TODO Auto-generated method stub

	}

	@Override
	public HashMap<AgentBodyID, Reward> getRewards() throws MissingResourceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void request(AgentMindID mind, Action myAction, ArtifactID artifact)
			throws MissingResourceException, SecurityException {
		// TODO Auto-generated method stub

	}

	@Override
	public void send(AgentMindID mind, Message myMessage, AgentMindID anotherMind)
			throws MissingResourceException {
		// TODO Auto-generated method stub

	}

}
