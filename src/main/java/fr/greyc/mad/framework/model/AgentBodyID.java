package fr.greyc.mad.framework.model;

import java.util.Objects;

public class AgentBodyID {
	private String bodyID;
	
	public AgentBodyID(String id) {
		bodyID = id;
	}
	
	public String getID() {
		return bodyID;
	}

	@Override
	public int hashCode() {
		return Objects.hash(bodyID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgentBodyID other = (AgentBodyID) obj;
		return Objects.equals(bodyID, other.bodyID);
	}
	
}
