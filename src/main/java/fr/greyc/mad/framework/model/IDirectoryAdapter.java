package fr.greyc.mad.framework.model;

import java.util.List;

public interface IDirectoryAdapter {
	List<Action> getActions(AgentBodyID bodyID);
	List<Observation<?>> getObservations(AgentBodyID bodyID);
};
