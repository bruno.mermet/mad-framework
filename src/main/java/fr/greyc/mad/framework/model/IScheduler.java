/**
 * 
 * IScheduler.java
 * Interface class for the MAD framework Scheduler.
 * 
 * @author Grégory Bonnet
 * @since 03-01-2022
 */

package fr.greyc.mad.framework.model;

import java.util.HashMap;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.concurrent.TimeoutException;

public interface IScheduler {

    /**
    * Set the DirectoryAdapter used to communicate with the AgentMind in
    * a transparent way.
    *
    * @param myDirectoryAdapter Specifies an adapter for communication.
    */
    public void setAdapter(IDirectoryAdapter myDirectoryAdapter);
    
    /**
    * Set the Environment with whom the Scheduler must communicate.
    *
    * @param myEnvironment Specifies the associated Environment.
    */
    public void register(Environment myEnvironment);
    
    /**
    * Register an agent as a controller for a given agent body. An agent
    * can register to several bodies, and a body can be controlled by 
    * several agents. The scheduler is not responsible for such situations.
    * No message or exception is raised if the body is already controlled
    * by the agent. An exception is thrown if the body does not exists in
    * the environment.
    *
    * This methods should be called by a DirectoryAdapter.
    *
    * @param mind Specifies the registered AgentMindID.
    * @param body Specifies the controlled AgentBodyID.
    * @throws MissingResourceException If the AgentBody is unavailable.
    * @throws SecurityException If the AgentMind declares an illegal control
    *                           with respect to the environment specification.
    */
    public void register(AgentMindID mind, AgentBodyID body) throws MissingResourceException, SecurityException;

    /**
    * Check if a given AgentBody ID exists in the environment.
    *
    * @param body Specifies the AgentBodyID.
    * @return boolean
    * @throws MissingResourceException If the Environment is not set.
    */
    public boolean isFound(AgentBodyID body) throws MissingResourceException;

    /**
    * Check if the environment is ready to be simulated given the current
    * set of controlled AgentBody (previously registred).
    *
    * @return boolean
    * @throws MissingResourceException If the environment is not set.
    */
    public boolean isEnvironmentReady() throws MissingResourceException;

    /**
    * Check if the all the registred AgentMind are ready to act within a
    * given delay. Setting the delay to zero means there is no delay
    * (i.e. this method is a blocking method). 
    * 
    * @param delay Specifies the delay in milliseconds.
    * @return boolean
    */
    public boolean isAllAgentMindsReady(int delay);

    /**
    * Check if a given AgentMind is ready to act. The AgentMind must answer
    * withing a given delay. Setting the delay to zero means there is no delay
    * (i.e. this method is a blocking method). 
    *
    * @param mind Specifies the registered AgentMindID.
    * @param dealy Specifies the delay in milliseconds.
    * @return boolean
    * @throws MissingResourceException If the AgentMind ID does not exist.
    * @throws TimeoutException If the AgentMind does not answer within the delay.
    */    
    public boolean isAgentMindReady(AgentMindID mind, int delay) throws MissingResourceException, TimeoutException;

    /**
    * Ask an AgentMind for its action for a given AgentBody. The AgentMind must be 
    * registred, must have declared to control the AgentBody, and must answer
    * withing a given delay. Setting the delay to zero means there is no delay
    * (i.e. this method is a blocking method). 
    * 
    * @param mind Specifies the registered AgentMindID.
    * @param body Specifies the controlled AgentBodyID.
    * @param delay Specifies the delay in milliseconds.
    * @return Action The Action requested by the AgentMindID.
    * @throws MissingRessourceException If the AgentMind ID or the AgentBody ID do not exists.
    * @throws SecurityException If the AgentMind provided an Action for an AgentBody it does not control.
    * @throws TimeoutException If the AgentMind does not answer within the delay.
    */
    public Action requestAction(AgentMindID mind, AgentBodyID body, int delay) throws MissingResourceException, SecurityException, TimeoutException;
    
    /**
    * Return a joint action built from the Action of the registred agents. An 
    * Action is associated to an AgentBody. An AgentBody that stays idle is 
    * associated to a null Action.
    * 
    * @return HashMap<String, Action> Associates an Agentbody ID with a given action.
    */
    public HashMap<String, Action> getNextJointAction();

    /**
    * Execute a given joint action. The joint action should be send to 
    * the Environment which executes it and updates its state.
    * 
    * @param HashMap<String, Action> Associates an AgentBody ID with a given action.
    * @throws MissingResourceException If the Environment does not exist.
    */
    public void executeJointAction(HashMap<AgentBodyID, Action> myJointAction) throws MissingResourceException;

    /**
    * Return a set of observation for each AgentBody. This method can be used
    * to get the initial state of the environment.
    *
    * @return HashMap<String, Set<Observation>> Associates an AgentBody ID with a given Set of Observations.
    * @throws MissingResourceException If the Environment does not exist.
    */
    public HashMap<AgentBodyID, Set<Observation>> getObservations() throws MissingResourceException;

    /**
    * Communicate to each registered AgentMind the Observation provided by the AgentBody
    * they control.
    *
    * @param myObservations An association between AgentBody and Set of Observation.
    */
    public void distributeObservations(HashMap<AgentBodyID, Set<Observation>> myObservations);

    /**
    * Return the reward for each AgentBody.
    *
    * @return HashMap<String, Reward> Associates an AgentBody ID with a given reward.
    * @throws MissingResourceException If the Environment does not exist.
    */
    public HashMap<AgentBodyID, Reward> getRewards() throws MissingResourceException;

    /**
    * Request the environment to execute the specific action issued by a given 
    * agent on a given artifact within the environment. 
    *
    * This methods should be called by the Directory Adapter.
    *
    * @param mind Specifies the AgentMind ID that request the action.
    * @param myAction Specifies the Action requested by the AgentMind.
    * @param artifact Specifies the Artifact ID within the Environment on which to apply the Action.
    * @throws MissingRessourceException If the AgentMind, the Artifact ID or the Environment do not exist.
    * @throws SecurityException If the AgentMind requested an illegal Action.
    */
    public void request(AgentMindID mind, Action myAction, ArtifactID artifact) throws MissingResourceException, SecurityException;

    /**
    * Send a given Message from a given AgentMind to another AgentMind.
    *
    * This method should be called by the DirectoryAdapter.
    *
    * @param mind Specifies the sender AgentMind.
    * @param myMessage Specifies the Message sent.
    * @param anotherMind Specifies the receiver AgentMind.
    * @throws MissingRessourceException If one of the agents does not exist.
    */
    public void send(AgentMindID mind, Message myMessage, AgentMindID anotherMind) throws MissingResourceException;

    /**
     * Give the list of actions an agent mind can perform.
     * An agent mind can perform agents associated to its body and 
     * agents available on the environment.
     * 
     * @param mind is the mind of the agent
     * @return the list of actions
     */
    public List<Action> getActions(AgentMindID mind);

    public List<Observation<?>> getObservations(AgentMindID mind);

}
