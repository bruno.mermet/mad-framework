package fr.greyc.mad.framework.model;

import java.util.List;

import fr.greyc.mad.framework.engine.agents.AgentMind;

public interface AgentBody {
	public List<Action> getActions();
	public List<Observation<?>> getObservations();
}
