package fr.greyc.mad.framework.model;

@FunctionalInterface
public interface Observation<E> {
	public E getValue();
}
