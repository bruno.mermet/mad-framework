package fr.greyc.mad.framework.model;

import java.util.Arrays;
import java.util.List;

public abstract class Environment {
	private List<AgentBody> bodies;
	
	public Environment() {
	}

	/**
	 * Returns a list of actions which can be performed on this environment but
	 * which do not affect any agent body (typically, require an artifact).
	 * 
	 * @return A list of actions which can be performed on this environment
	 */
	protected abstract List<Action> getEnvironmentActions();

	protected abstract List<Observation<?>> getEnvironmentObservations();

	public void next() {

	}

	public void next(List<Action> actions) {
		for (Action action : actions) {
			action.execute();
		}
		next();
	}
	
	/**
	 * Method to add one or more agent bodies to the environment.
	 * 
	 * @param newBodies the bodies to add
	 */
	public void addBodies(AgentBody... newBodies) {
		Arrays.stream(newBodies).forEach(agentBody -> bodies.add(agentBody));
	}
}
