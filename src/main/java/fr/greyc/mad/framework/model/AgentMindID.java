package fr.greyc.mad.framework.model;

import java.util.Objects;

public class AgentMindID {
	private String mindID;
	
	public AgentMindID(String id) {
		mindID = id;
	}
	
	public String getID() {
		return mindID;
	}

	@Override
	public int hashCode() {
		return Objects.hash(mindID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgentMindID other = (AgentMindID) obj;
		return Objects.equals(mindID, other.mindID);
	}
	
}
