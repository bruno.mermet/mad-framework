package fr.greyc.mad.framework.model;

public interface Action {
	public void execute();
}
