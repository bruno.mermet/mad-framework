package fr.greyc.mad.framework.example.model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.greyc.mad.framework.engine.agents.AgentMind;
import fr.greyc.mad.framework.model.Action;
import fr.greyc.mad.framework.model.AgentBody;
import fr.greyc.mad.framework.model.Observation;

public class PreyBody implements AgentBody {
	private int x;
	private int y;
	
	public PreyBody(AgentMind mind, int x0, int y0) {
		x = x0;
		y = y0;
	}

	@Override
	public List<Action> getActions() {
		Action right = () -> {x += 1;};
		Action left = () -> { x-= 1;};
		Action up = () -> {y += 1;};
		Action down = () -> {y -= 1;};
		return Stream.of(right, left, up, down)
				     .collect(Collectors.toList());	
	}

	@Override
	public List<Observation<?>> getObservations() {
		Observation<Integer> obsX = () -> x;
		Observation<Integer> obsY = () -> y;
		return Stream.of(obsX, obsY)
				     .collect(Collectors.toList());
	}
	
	
}
